files = ["hdl/rx_core/ber.sv",
         "hdl/rx_core/ber_scrambler.v",
         "hdl/rx_core/bitslip_fsm.sv",
         "hdl/rx_core/block_sync.v",
         "hdl/rx_core/delay_controller_wrap.v",
         "hdl/rx_core/descrambler.v",
         "hdl/rx_core/gearbox_32_to_66.v",
         "hdl/rx_core/lcd_driver.vhd",
         "hdl/rx_core/serdes_1_to_468_idelay_ddr.v",
         "hdl/ip_core/clk_wiz_3.xcix",
         "hdl/ip_cores/ila_1.xcix",
         "hdl/ip_cores/vio_0.xcix"
         ]

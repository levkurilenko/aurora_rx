// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4.1 (win64) Build 2117270 Tue Jan 30 15:32:00 MST 2018
// Date        : Mon May 14 20:39:02 2018
// Host        : LAPTOP-04A655BG running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/research/aurora_rx/aurora_rx.runs/fifo_fwft_synth_1/fifo_fwft_stub.v
// Design      : fifo_fwft
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_2_1,Vivado 2017.4.1" *)
module fifo_fwft(clk, rst, din, wr_en, rd_en, dout, full, empty)
/* synthesis syn_black_box black_box_pad_pin="clk,rst,din[65:0],wr_en,rd_en,dout[65:0],full,empty" */;
  input clk;
  input rst;
  input [65:0]din;
  input wr_en;
  input rd_en;
  output [65:0]dout;
  output full;
  output empty;
endmodule

// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4.1 (win64) Build 2117270 Tue Jan 30 15:32:00 MST 2018
// Date        : Fri May 11 15:03:50 2018
// Host        : LAPTOP-04A655BG running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub D:/research/aurora_rx/aurora_rx.runs/vio_0_synth_1/vio_0_stub.v
// Design      : vio_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2017.4.1" *)
module vio_0(clk, probe_out0, probe_out1, probe_out2, 
  probe_out3)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_out0[0:0],probe_out1[31:0],probe_out2[0:0],probe_out3[0:0]" */;
  input clk;
  output [0:0]probe_out0;
  output [31:0]probe_out1;
  output [0:0]probe_out2;
  output [0:0]probe_out3;
endmodule

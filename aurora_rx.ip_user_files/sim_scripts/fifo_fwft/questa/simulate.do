onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fifo_fwft_opt

do {wave.do}

view wave
view structure
view signals

do {fifo_fwft.udo}

run -all

quit -force
